<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>PHP & HTML</title>
  </head>
  <body>
    <h1>Liste des élèves</h1>
    <!-- Instructions : Afficher la liste des éléves qui sont présent dans le tableau $students -->
    <?php
        //students
        $students = ['Hulk', 'Iron Man', 'Wonder Woman', 'Black Widow', 'Malicia'];
        ?>
     <ul>
       <?php 
        print_r($students);
     //display the students here ?>
     </ul>
     <hr>
     <h1>Date du jour</h1>
     <form>

           <!-- Instructions : Créer la liste de jour (en chiffres), de mois (en chiffres) et d'année en PHP. -->
           <label for="day">Day</label>
           <select  name="day" id="day"><?php 
             $selected = '';
                      for($i=1; $i<=31; $i++)
                      {
                        echo "\t",'<option value="', $i ,'"', $selected ,'>', $i ,'</option>',"\n";
                        $selected='';
                      }
                            //list of day ?></select>
           <label for="month">Month</label>
       <select  name="month" id="month"><?php 
       $selected = '';
                      for($i=1; $i<=12; $i++)
                      {
                        echo "\t",'<option value="', $i ,'"', $selected ,'>', $i ,'</option>',"\n";
                        $selected='';
                      }
        //list of month ?></select>
       <label for="year">Year</label>
       <select  name="year" id="year"><?php 
       $selected = '';
                      for($i=1900; $i<=2019; $i++)
                      {
                        echo "\t",'<option value="', $i ,'"', $selected ,'>', $i ,'</option>',"\n";
                        $selected='';
                      }
      //list of year ?></select>
     </form>
     <hr>
     <?php 
     $fille = Fille;
     $garcon = Garcon;
     $autre = Autre;
     if ( $fille == "Fille"){
      echo "Je suis une fille";
     }
     elseif ( $garcon == "Garcon"){
      echo "Je suis un garçon";
     }
     elseif ( $autre == "Autre"){
      echo "Je suis indéfini";
     }
     ?>
     <!-- Instruction : Afficher ce bloc que si dans l'URL il y'a une variable sexe et que ça valeur vaut "fille" -->
    
     <!-- Instruction : Afficher ce bloc que si dans l'URL il y'a une variable sexe et que ça valeur vaut "garçon" -->
    
     <!-- Instruction : Afficher ce bloc dans les autres cas -->

  </body>
</html>
